#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("gnu");
    QCoreApplication::setOrganizationDomain("gnu.org");
    QCoreApplication::setApplicationName("QLogTimeliner");


    MainWindow w;
    w.show();

    return a.exec();
}
