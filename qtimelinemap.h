#ifndef QTIMELINEMAP_H
#define QTIMELINEMAP_H

#include <QWidget>
#include "logevent.h"

class qtimelinemap : public QWidget
{
    Q_OBJECT
public:
    explicit qtimelinemap(QWidget *parent = 0);

    QPair<QDateTime, QDateTime> getSelectedRange() const;

signals:

public slots:

    void setVisibleRange(const QDateTime &left, const QDateTime &right);
    void updateWithEvents(const SeriesMap &seriesMap);

private slots:
    void onContextMenuRequested(const QPoint& pos);

private:

    void wheelEvent(QWheelEvent* we);
    void paintEvent(QPaintEvent *pe);

    QPixmap pm;
    QPair<QDateTime, QDateTime> visible;
    QPair<QDateTime, QDateTime> minMax;
    QPair<long, long> viewport;
    QPair<qreal, qreal> inrange;

};

#endif // QTIMELINEMAP_H
