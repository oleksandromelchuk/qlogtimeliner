Your application log analysis tool.

This tool helps with analysis multithreaded logs by visualising log events on a timeline.
It is user responsibility to create rules for log parsing in actionscript (like JavaScript)

https://gitlab.com/oleksandromelchuk/qlogtimeliner
http://qlogtimeliner.sourceforge.net - deprecated

![Screenshot](qlogtimeliner.png)

Author Oleksandr Omelchuk 


License GPL v3


How to build:

mkdir build
cmake ..
cmake --build

Or
qmake && make


How to use:

Check out example log nad analysis script.

1. Open yout log
2. Prse log lines and return list of events
3. Examine events table
4. Build timeline
5. Selecting event on timeline will select corresponding line in Log view tab for detailed examination.
