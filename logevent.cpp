#include "logevent.h"

QString LogEvent::description() const
{
    QString s("Timestamp: ");
    s.reserve(400);
    s.append(timestamp.toString());
    s.append("\nLine: ");
    s.append(line);
    s.append("\nPosition: ");
    s.append(position);
    return s;
}
