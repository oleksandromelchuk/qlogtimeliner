
// debug("Logs this message to app console")

// if no log supplied this log will be used http://easylogging.org/sample.log
var exampleLog = (function () {/*
...
12/02/2014 10:57:55,592 INFO  [settings_loader] Saving settings to [/home/mkhan/project-islam-data/basic_settings.ini]
12/02/2014 10:57:55,592 VER-9 [settings_loader] Saving setting [update_checked]
12/02/2014 10:57:56,149 DEBUG [quran] Executed [Load Complete Quran] in [532 ms]
12/02/2014 10:57:57,689 INFO  [default] Loading all the extensions. ExtensionBar [0xc02810]; application path: /home/mkhan/cpp/project-islam-build
12/02/2014 10:57:57,701 INFO  [extension.AlQuran] Changing settings file to [/home/mkhan/project-islam-data/basic_settings.ini]
12/02/2014 10:57:57,734 INFO  [extension.AlQuran] Loading chapter [Al-Fatihah]
12/02/2014 10:57:57,734 INFO  [extension.loader] Loading [http://subres.com]
12/02/2014 10:57:57,800 DEBUG [extension.performance.AlQuran] Executed [void QuranView::updateView()] in [2 ms]
12/02/2014 10:57:57,800 DEBUG [extension.performance.AlQuran] Executed [void QuranView::update(quran::Chapter *, int, int)] in [66 ms]
12/02/2014 10:57:57,800 DEBUG [extension.performance.AlQuran] Executed [void QuranReader::on_cboChapter_currentIndexChanged(int)] in [66 ms]
12/02/2014 10:57:57,804 INFO  [extension.loader] Loading [http://jquery.com]
12/02/2014 10:57:57,804 INFO  [extension.AlQuran] Adding bookmark [Surah Kahf - Recite=18:32-44]
12/02/2014 10:57:57,804 INFO  [extension.AlQuran] Adding bookmark [Surah Humazah=104:1-9]
12/02/2014 10:57:57,844 INFO  [extension.loader] Finished [http://subres.com]
12/02/2014 10:57:57,804 INFO  [extension.AlQuran] Adding bookmark [Memorization=90:1-20:5]
12/02/2014 10:57:57,804 INFO  [extension.AlQuran] Saving settings to [/home/mkhan/project-islam-data/basic_settings.ini]
12/02/2014 10:57:57,853 DEBUG [extension.performance.AlQuran] Executed [void QuranView::updateView()] in [2 ms]
12/02/2014 10:57:57,853 DEBUG [extension.performance.AlQuran] Executed [void QuranView::update(quran::Chapter *, int, int)] in [41 ms]
12/02/2014 10:57:57,853 INFO  [extension.loader] Got 57 bytes [http://jquery.com]
12/02/2014 10:57:57,853 INFO  [extension.AlQuran] Saving settings to [/home/mkhan/project-islam-data/basic_settings.ini]
12/02/2014 10:57:57,889 DEBUG [extension.performance.AlQuran] Executed [void QuranView::updateView()] in [2 ms]
12/02/2014 10:57:57,889 DEBUG [extension.performance.AlQuran] Executed [void QuranView::update(quran::Chapter *, int, int)] in [35 ms]
12/02/2014 10:57:57,889 INFO  [extension.AlQuran] Saving settings to [/home/mkhan/project-islam-data/basic_settings.ini]
12/02/2014 10:57:57,897 DEBUG [extension.performance.AlQuran] Executed [void QuranView::updateView()] in [2 ms]
12/02/2014 10:57:57,897 DEBUG [extension.performance.AlQuran] Executed [void QuranView::update(quran::Chapter *, int, int)] in [7 ms]
12/02/2014 10:57:57,897 INFO  [extension.AlQuran] Saving settings to [/home/mkhan/project-islam-data/basic_settings.ini]
12/02/2014 10:57:57,909 DEBUG [default] [void SettingsLoader::changeSettingsFile(const QString &)] Logger [extension.AlQuran] is not registered yet!
12/02/2014 10:57:57,914 INFO  [extension.Salah] Building salah times for 12/2/2,014 @ (-35.282, 149.129) timezone: 11
12/02/2014 10:57:57,915 INFO  [extension.loader] Finished [http://jquery.com]
12/02/2014 10:57:57,969 DEBUG [performance] [el::base::Trackable::~Trackable()] Executed [void MainWindow::reloadStyles()] in [48 ms]
...
*/}).toString();

var dateRegEx = /(\d+)\/(\d+)\/(\d+)\s+(\d+):(\d+):(\d+)\,(\d+)/;
var linesSplitter = /(\d+\/\d+\/\d+\s+\d+:\d+:\d+\,\d+)/g ;


// Creates basic event
timestampedObject = function(line) {
    var obj = {};
    var str = dateRegEx.exec(line);
    debug(str);
    obj.timestamp = new Date(+str[1], +str[2], +str[3], +str[4], +str[5], +str[6], +str[7], +str[8]);
    obj.line = line.substring(0, line.length-1); // strange trailing space
    obj.series = "main";
    obj.position = "";  // [Begin, Mid, End]
    return obj;
}


allEvents = function(line) {
    debug("allEvets "+line);
    var obj = timestampedObject(line);
    var jobName = /\s\[([a-z,\.]*)\]\s/i.exec(line)[1];
    obj.series = jobName;
    return obj;
}


socket = function(line) {
    var obj = timestampedObject(line);
    var url = /\s\[(http.*)\]/.exec(line)
    obj.relation = url[1]
    if (/Loading\s/.test(line))  obj.position = "Begin";
    if (/\sGot\s\d+\sbytes/.test(line))  obj.position = "Mid";
    if (/\sFinished\s/.test(line))  obj.position = "End";
    obj.series = "sockets";
    return obj;
}

errGeneral = function(line) {
    // filter false positives:
    if (/Not an error: 0/i.test(line)) return null;

    var obj = timestampedObject(line);
    obj.series = "Error";
    return obj;
}

// let's have regexp based matchers for this case:
var types = [];
types.push([ /.*/i, allEvents]);

types.push([ /error/i, errGeneral]);
types.push([ /warning/i, errGeneral]);
types.push([ /fail/i, errGeneral]);

types.push([ /extension.loader/i, socket]);

// single line parser
getEventFromLine = function(line)
{
    var objs = [];
    for (var i =0; i <types.length; ++i) {
        if (types[i][0].test(line)) {
            var f = types[i][1];
            var obj = f(line);
            if (obj) objs.push(obj);
        }
    }
    return objs;
};


// Entry point. Special function which is responcible for translating log string to list of events.
getEventsFromLog = function(log)
{
    if (log.length == 0) {
        log = exampleLog;
        debug(exampleLog);
    }
    var lines = log.split(linesSplitter);
    var objs = [];
    var i = 0;
    if (linesSplitter.test(lines[1])) i=1;
    for ( ; i < lines.length; i+=2) {
        if ((i % 200) < 2) debug("Parsed: "+(i/lines.length)*100.0+" %");
        try {
            var o = getEventFromLine(lines[i]+lines[i+1]);
            for (var n=0; n<o.length; ++n) {
                objs.push(o[n]);
            }
        } catch(e) {
            debug(e);
        }
    }
        return objs;
}



