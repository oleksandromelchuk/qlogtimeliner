#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QJSEngine>
#include <QJSValue>
#include <QScopedPointer>
#include <QPointer>
#include <QMap>
#include <QList>
#include <QRandomGenerator>
#include "logevent.h"

namespace Ui {
class MainWindow;
}

class QGraphicsScene;
class QScriptEngine;
class QGraphicsTextItem;
class QGraphicsRectItem;
class QScriptContext;


class DebugExtension: public QObject
{
    Q_OBJECT
public:
    Q_INVOKABLE void debug(QString value);
};


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void saveSettings();

protected:
    void makeNewEpoch();

    void updateTimelineRangeStrings();

    void clearGraphicsScene();

private:
    void resetEngine();
    void mapToSeries(const QSharedPointer<LogEvent>& le);
    void drawSeries(const ListOfEventGroups& list, QGraphicsScene *scene, const qreal& zoom, const QPair<QDateTime, QDateTime>& filterRange);
    void drawGroupLine(const QColor& seriesBrush, const QRectF& rect,const QString& description, QGraphicsScene *scene, const qreal& zoom, const EventsGroup& eg);
    void drawEventsGroup(const qreal& seriesTop, QVector<qreal>& seriesRows, const qreal& zoom, QGraphicsScene *scene, const EventsGroup& eg, const QColor& seriesBrush, const QPair<QDateTime, QDateTime>& filterRange);
    void drawSingleEvent(QGraphicsScene *scene, const qreal& seriesTop, const qreal& zoom, const EventsGroup& eg, const QColor& seriesBrush);
    void drawRealGroup(QGraphicsScene *scene, const qreal& seriesTop, QVector<qreal>& seriesRows, const qreal& zoom, const EventsGroup& eg, const QColor& seriesBrush);
    QGraphicsRectItem * createGraphicsItem(const qreal& y, const qreal& width, const LogEventP& le, const qreal& height, const qreal& seriesTop, const QColor& seriesBrush);
    void addTableItem(const LogEventP& le);
    const QDateTime createLogEventFromJS(const QJSValue& val);

    quint32 parseJSEvents(QJSValue &gfunc);
    void buildEventsMap();
    QStringList getDescription(qreal left, const EventsGroup& eg, const qreal& zoom, qreal right);

private slots:
    void checkAS();
    void on_graphicsSelectionChanged();

    void onSignalHandlerException(const QJSValue & exception);

    void on_loadLogButton_clicked();

    void on_rulesTextEdit_textChanged();

    void on_buildButton_clicked();

    void on_zoomSlider_valueChanged(int value);

    void on_tabWidget_currentChanged(int index);

    void on_graphicsView_rubberBandChanged(const QRect &viewportRect, const QPointF &fromScenePoint, const QPointF &toScenePoint);

    void onSceneRectChanged(const QRectF & rect);

    void onHorizontalScroll(int pos);

    void on_plainTextEdit_textChanged();

    void on_findNextButton_clicked();

    void on_findPrevButton_clicked();

    void on_findLineEdit_returnPressed();

    void on_stopButton_clicked();

    void on_epochBox_activated(int index);

private:
    void closeEvent(QCloseEvent *event);

    Ui::MainWindow *ui;
    QScopedPointer<QJSEngine> engine;
    QPointer<QGraphicsTextItem> buble;
    QString log;
    bool mNeedToCheck, mNeedToRebuild, mStopped;

    QRandomGenerator mRandom;
    EpochList epochList;
    /// remember which Epoch is visible
    void *visibleSeriesID = nullptr;
};

#endif // MAINWINDOW_H
