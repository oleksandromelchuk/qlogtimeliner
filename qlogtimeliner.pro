#-------------------------------------------------
#
# Project created by QtCreator 2014-10-01T19:52:11
#
#-------------------------------------------------

QT       += core gui qml positioning

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qlogtimeliner
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    logevent.cpp \
    qtimelinemap.cpp \
    jsedit/jsedit.cpp

HEADERS  += mainwindow.h \
    qtimelinemap.h \
    jsedit/jsedit.h \
    logevent.h

FORMS    += mainwindow.ui

#QMAKE_MAC_SDK = macosx10.9

OTHER_FILES += \
    logevent.h \
    example.log \
    example.js \
    Readme.txt

RESOURCES += \
    resource.qrc

