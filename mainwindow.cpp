#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QSettings>
#include <QJSEngine>
#include <QTimer>
#include <QFileDialog>
#include <QTextStream>
#include <QDebug>
#include <QScrollBar>
#include <QGraphicsRectItem>
#include "logevent.h"

static QTextEdit *jsEdit = nullptr;

void DebugExtension::debug(QString value)
{
        qDebug() << "JS>> " << value;
        jsEdit->append( QString("JS>> %0").arg(value) );
        QApplication::processEvents();
}



void MainWindow::resetEngine()
{
    engine.reset(new QJSEngine(this));

    engine->globalObject().setProperty("native", engine->newQObject(new DebugExtension));
    engine->evaluate("function debug(a) {native.debug(a);}"); // backward compatibility
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    jsEdit = ui->jsLog;

    QObject::connect(ui->graphicsView->horizontalScrollBar(), SIGNAL(valueChanged(int)),
                     this, SLOT(onHorizontalScroll(int)));

    resetEngine();

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(checkAS()));
    timer->start(3000);

    qDebug() << "loading";
    QSettings settings;
    ui->rulesTextEdit->setPlainText(settings.value("script").toString());
    if (ui->rulesTextEdit->toPlainText().isEmpty()) {
        QFile data(":/txt/example.js");
        if (data.open(QFile::ReadOnly | QIODevice::Text)) {
            ui->rulesTextEdit->setPlainText(QString::fromUtf8(data.readAll()));
        }
    }
    ui->progressBar->hide();
    ui->mainProgressBar->hide();
    ui->stopButton->hide();
    ui->tabWidget->setCurrentIndex(0);
    ui->jsLog->hide();
}

void MainWindow::saveSettings()
{
    qDebug() << "saving";
    QSettings settings;
    settings.setValue("script", ui->rulesTextEdit->toPlainText());
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    saveSettings();
    QMainWindow::closeEvent(event);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_loadLogButton_clicked()
{
    const QString fname = QFileDialog::getOpenFileName(this, "Select log");

    if (!fname.isEmpty()) {
        setWindowTitle(QString("QLogTimeliner %1").arg(fname));
        QFile data(fname);
        if (data.open(QFile::ReadOnly | QIODevice::Text)) {
            ui->logWidget->setRowCount(0);
            const QByteArray bt = data.readAll();
            log = QString::fromStdString(bt.toStdString());
            ui->plainTextEdit->setPlainText(log);
        }
    }
    mNeedToRebuild = true;
}

void MainWindow::on_rulesTextEdit_textChanged()
{
    mNeedToCheck = true;
    mNeedToRebuild = true;
}

void MainWindow::on_plainTextEdit_textChanged()
{
    mNeedToRebuild = true;
}

void MainWindow::on_graphicsSelectionChanged()
{
    if (ui->graphicsView->scene() == nullptr)
        return;

    const QList<QGraphicsItem *>selectedItems = ui->graphicsView->scene()->selectedItems();

    if (selectedItems.count() == 1) {
        QGraphicsItem * selected = selectedItems.first();
        const QString description = selected->data(1).toString();
        const QString line        = selected->data(2).toString();
        if (buble.isNull()) {
            buble = ui->graphicsView->scene()->addText(description);
        }  else {
            buble->setPlainText(description);
            if (buble->scene() != selected->scene() ) {
                selected->scene()->addItem(buble);
            }
        }

        buble->setTextWidth(qMax(ui->graphicsView->width()*0.7, 300.0));
        buble->setPos(selected->sceneBoundingRect().bottomLeft() + QPointF(2, 2));
        buble->setTransform(QTransform::fromScale(1.0/(static_cast<qreal>(ui->zoomSlider->value())/(ui->zoomSlider->maximum())), 1.0));
        buble->show();

        ui->plainTextEdit->moveCursor(QTextCursor::Start);
        ui->plainTextEdit->find(line);
        ui->plainTextEdit->ensureCursorVisible();
    } else {
        if (!buble.isNull())
            buble->hide();
    }
}

void MainWindow::makeNewEpoch()
{
    epochList.push_back(SeriesMap());
    ui->epochBox->addItem(QString("Epoch %0").arg(epochList.length()));
}

void MainWindow::mapToSeries(const LogEventP& le)
{

    const QString &series = le->series;
    if (le->isEpoch() && !epochList.isEmpty() && !epochList.first().isEmpty()) {
        makeNewEpoch();
    }

    SeriesMap &seriesMap = epochList.last();
    ListOfEventGroups seriesEvents = seriesMap.value(series, ListOfEventGroups(new QList<EventsGroup>()));


    bool inserted = false;
    if (le->isInRelation()) {

        if( !le->isBegin() && !seriesEvents->isEmpty()) {
//          qDebug() << "attempt to merge";
            // find group for this item
            QList< EventsGroup>::const_iterator it = seriesEvents->constEnd();
            do {
                --it;
                const EventsGroup& currentGroup = *it;
                if (!currentGroup->isEmpty()) {
                    const LogEventP& lastEvent = currentGroup->last();
                    if (lastEvent->relation == le->relation && !lastEvent->isEnd()) {
                        currentGroup->push_back(le);
    //                    qDebug() << "merging relation "<< le->relation << " count " << currentGroup->count();
                        inserted = true;
                        break;
                    }
                }
            } while (it > seriesEvents->constBegin());
        }
    }

    if (!inserted) {
//        qDebug() << "adding new";
        EventsGroup grouppedEvent(new QList<LogEventP>());
        grouppedEvent->append(le);
        seriesEvents->append(grouppedEvent);
    }
    seriesMap.insert(series, seriesEvents);
}

void MainWindow::checkAS()
{
    if (mNeedToCheck) {
        const QString as = ui->rulesTextEdit->toPlainText();
        QJSEngine validator;
        QJSValue result = validator.evaluate(as);
        if (!result.isError()) {
            ui->errorsLog->hide();
            ui->tab_3->setEnabled(true);
        } else {
            ui->errorsLog->show();
            ui->tab_3->setEnabled(false);
            ui->errorsLog->setPlainText(result.toString());
        }
        mNeedToCheck = false;
    }
}

QGraphicsRectItem * MainWindow::createGraphicsItem(const qreal& y, const qreal& width, const LogEventP& le, const qreal& height, const qreal& seriesTop, const QColor& seriesBrush)
{
    QGraphicsRectItem *ri = new QGraphicsRectItem(y, seriesTop, width, height);
    ri->setPen(QPen(Qt::blue));
    ri->setBrush(seriesBrush);
    ri->setData(1, QVariant::fromValue(le->description()));
    ri->setData(2, QVariant::fromValue(le->line));
    ri->setFlags(ri->flags() | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable);

    return ri;
}

inline qreal msecFromDT(const QDateTime& dt)
{
    return dt.time().msecsSinceStartOfDay();// + 24.0*60*60*1000* dt.date().day();
}

static const qreal vspacing = 4.0;
static const qreal gwidth = 40.0;
static const qreal gheight = 30.0;

void MainWindow::drawSingleEvent(QGraphicsScene *scene, const qreal& seriesTop, const qreal& zoom, const EventsGroup& eg, const QColor &seriesBrush)
{
    const LogEventP le = eg->first();
    if (le->timestamp.isValid()) {
        const qreal y = msecFromDT(le->timestamp)*zoom;
        QGraphicsRectItem *ri = createGraphicsItem(y, gwidth, le, gheight, seriesTop, seriesBrush);
        scene->addItem(ri);
    }
}

void MainWindow::drawGroupLine(const QColor& seriesBrush, const QRectF& rect, const QString& description, QGraphicsScene *scene, const qreal& zoom, const EventsGroup& eg)
{
    QGraphicsRectItem *ri = scene->addRect(rect, QPen(Qt::red), seriesBrush);
    ri->setData(1, QVariant::fromValue(description));
    ri->setData(2, eg->first()->line);
    ri->setFlags(ri->flags() | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable);

    QGraphicsTextItem *ti = new QGraphicsTextItem(eg->first()->relation);
    ti->setParentItem(ri);
    ti->setPos(rect.topLeft());

    QListIterator<LogEventP> ei(*eg);
    while (ei.hasNext() && !mStopped) {
        const LogEventP le = ei.next();
        if (!le.isNull() && le->timestamp.isValid()) {
            const qreal y = msecFromDT(le->timestamp)*zoom;
            const qreal hheight = gheight/2.0;
            const qreal top = rect.top() + (gheight - hheight)/2.0;
            QGraphicsRectItem *rp = createGraphicsItem(y, gwidth, le, hheight, top, seriesBrush);
            rp->setParentItem(ri);
            rp->setOpacity(0.5);
        }
    }
}

// search for free slot for new event
int getRow(const qreal& left, QVector<qreal>& seriesRows)
{
    int row = -1;
    for (int i = 0; i< seriesRows.size(); ++i) {
        if (seriesRows.at(i) < left) {
            row = i;
            break;
        }
    }
    if (row < 0) {
        seriesRows.append(left);
        row = seriesRows.size()-1;
    }

    return row;
}

void MainWindow::drawRealGroup(QGraphicsScene *scene, const qreal& seriesTop, QVector<qreal>& seriesRows,const qreal& zoom, const EventsGroup& eg, const QColor& seriesBrush)
{
    qreal left = msecFromDT(eg->first()->timestamp)*zoom;
    qreal right = left;

    QStringList descriptions;
    {
        QListIterator<LogEventP> ei(*eg);
        while (ei.hasNext() && !mStopped) {
            const LogEventP le = ei.next();
            if (le->timestamp.isValid()) {
                const qreal y = msecFromDT(le->timestamp)*zoom;
                left = qMin(y, left);
                right = qMax(y, right);

                descriptions << le->description();
            }
        }
    }

    // get top based on rows
    const int row = getRow(left, seriesRows);
    const qreal top = seriesTop + (row+1)*(gheight+vspacing);
    const QRectF rect(left, top, right-left+gwidth, gheight);
    seriesRows[row] = rect.right();
    drawGroupLine(seriesBrush, rect, descriptions.join('\n'), scene, zoom, eg);
}

void MainWindow::drawEventsGroup(const qreal& seriesTop, QVector<qreal>& seriesRows, const qreal& zoom, QGraphicsScene *scene, const EventsGroup& eg, const QColor &seriesBrush, const QPair<QDateTime, QDateTime> &filterRange)
{
    //qDebug() << "    drawEventsGroup()";
    if (eg->first() && eg->first()->timestamp <= filterRange.second && eg->last() && eg->last()->timestamp >= filterRange.first)
    {
        if (eg->count() == 1) {
            drawSingleEvent(scene, seriesTop, zoom, eg, seriesBrush); //  draws only on first row
        } else if (eg->count()>1) {
            drawRealGroup(scene, seriesTop+gheight+vspacing, seriesRows, zoom, eg, seriesBrush); // draws starting from second row
        }
    } else {
        //qDebug() << "              groupFiltered";
    }
}

void MainWindow::drawSeries(const ListOfEventGroups& list, QGraphicsScene *scene, const qreal& zoom, const QPair<QDateTime, QDateTime> &filterRange)
{
    const qreal seriesTop = scene->sceneRect().bottom();
    QVector<qreal> seriesRows; // right coordinates of last element in row
    QListIterator<EventsGroup> gi(*list);
    const int one = 180;
    const int two = 255-one;
    const QColor seriesBrush(mRandom.bounded(two)+one, mRandom.bounded(two)+one, mRandom.bounded(two)+one);

    int progress = 0;
    ui->progressBar->setMaximum(list->count());
    while (gi.hasNext() && !mStopped) {
        const EventsGroup eg = gi.next();
        drawEventsGroup(seriesTop, seriesRows, zoom, scene, eg, seriesBrush, filterRange);
        ui->progressBar->setValue(++progress);
        QApplication::processEvents();
    }
    qDebug() << "drawSeries() with rows "<< seriesRows.size();
}

void MainWindow::updateTimelineRangeStrings()
{
    const QPair<QDateTime, QDateTime> filterRange = ui->timelineMap->getSelectedRange();
    qDebug()<< "filterRange = "<< filterRange;
    ui->leftLabel->setText(filterRange.first.toString());
    ui->rightLabel->setText(filterRange.second.toString());
}

void MainWindow::clearGraphicsScene()
{
    QGraphicsScene *scene = ui->graphicsView->scene();
    ui->graphicsView->setScene(nullptr);
    delete scene;
}

void MainWindow::on_buildButton_clicked()
{
    qDebug() << "building chart";
    mStopped = false;
    ui->buildButton->hide();
    ui->stopButton->show();
    ui->progressBar->show();

    const SeriesMap &seriesMap = epochList.at(ui->epochBox->currentIndex());

    ui->mainProgressBar->setVisible(seriesMap.size()>1);
    const qreal zoom = 2.0;

    clearGraphicsScene();

    QGraphicsScene *scene = new QGraphicsScene();
    QObject::connect(scene, SIGNAL(selectionChanged()),
                     this, SLOT(on_graphicsSelectionChanged()));
    QObject::connect(scene, SIGNAL(sceneRectChanged(QRectF)),
                     this, SLOT(onSceneRectChanged(QRectF)));

    updateTimelineRangeStrings();
    const QPair<QDateTime, QDateTime> filterRange = ui->timelineMap->getSelectedRange();

    QMapIterator<QString, ListOfEventGroups> gri(seriesMap);
    ui->mainProgressBar->setMaximum(seriesMap.size());
    ui->mainProgressBar->setValue(0);

    while(gri.hasNext() && !mStopped) {
        gri.next();

        const QString sname = gri.key();
        const ListOfEventGroups list = gri.value();

        drawSeries(list, scene, zoom, filterRange);
        ui->mainProgressBar->setValue(ui->mainProgressBar->value()+1);
    }

    ui->graphicsView->setScene(scene);
    ui->timelineMap->updateWithEvents(seriesMap);
    ui->stopButton->hide();
    ui->progressBar->hide();
    ui->mainProgressBar->hide();
    ui->buildButton->show();
    visibleSeriesID = const_cast<SeriesMap *>(&seriesMap);
}

void MainWindow::on_stopButton_clicked()
{
    mStopped = true;
}

void MainWindow::onSignalHandlerException(const QJSValue &exception)
{
    qDebug() << "handle exception: \n" <<exception.toString() ;
}

void MainWindow::on_zoomSlider_valueChanged(int value)
{
    ui->graphicsView->setTransform(QTransform::fromScale(static_cast<qreal>(value)/(ui->zoomSlider->maximum()), 1.0));
}

void MainWindow::addTableItem(const LogEventP& le)
{
    const int lastRow = ui->logWidget->rowCount();
    ui->logWidget->setRowCount(lastRow+1);
    ui->logWidget->setItem(lastRow, 0, new QTableWidgetItem(le->timestamp.toString()));
    ui->logWidget->setItem(lastRow, 1, new QTableWidgetItem(le->relation));
    ui->logWidget->setItem(lastRow, 2, new QTableWidgetItem(le->position));
    ui->logWidget->setItem(lastRow, 3, new QTableWidgetItem(le->series));
    ui->logWidget->setItem(lastRow, 4, new QTableWidgetItem(le->line));
}

const QDateTime MainWindow::createLogEventFromJS(const QJSValue& val)
{
    const QDateTime timestamp = val.property("timestamp").toDateTime();

    return timestamp;
}

quint32 MainWindow::parseJSEvents(QJSValue &gfunc)
{
    QJSValueList args;
    args << ui->plainTextEdit->toPlainText();
    const QJSValue les = gfunc.call(args);
    const quint32 length = les.property("length").toUInt();
    qDebug() << " length " << length;
    QDateTime lastTimestamp;

    for (quint32 i = 0; i != length; ++i)
    {
        if (i % 500==0) {
            qDebug() << "Merged "<<  (100.0*i)/length << " %";
            ui->jsLog->append(QString("Merged %0 %").arg((100.0*i)/length, 0, 'g', 2));
            QApplication::processEvents();
        }
        const QJSValue val = les.property(i);
        const QDateTime timestamp = createLogEventFromJS(val);
        if (timestamp.isValid()) {
            LogEventP le(new LogEvent);
            le->timestamp = timestamp;
            le->line = val.property("line").toString();
            le->series = val.property("series").toString();
            le->relation = val.property("relation").toString();
            le->position = val.property("position").toString();

            mapToSeries(le);
            addTableItem(le);
            if (lastTimestamp.isValid() && lastTimestamp > le->timestamp) {
                qDebug()<<"ERR "<< le->timestamp << " is greater than new timestamp "<< le->timestamp;
            }
        }
    }

    const SeriesMap &seriesMap = epochList.at(ui->epochBox->currentIndex());
    ui->timelineMap->updateWithEvents(seriesMap);
    return length;
}

void MainWindow::buildEventsMap()
{
    resetEngine();

    epochList.clear();
    ui->epochBox->clear();
    makeNewEpoch();

    ui->errorsLog->clear();
    ui->logWidget->setRowCount(0);
    ui->jsLog->show();
    ui->tabWidget->hide();
    QApplication::processEvents();
    ui->jsLog->append("\n Building new events ...\n");

    const QString as = ui->rulesTextEdit->toPlainText();
    const QJSValue result = engine->evaluate(as);
    if (result.isError()) onSignalHandlerException(result);

    QJSValue gfunc = engine->evaluate(QString("getEventsFromLog"));
    if (gfunc.isError()) {
        onSignalHandlerException(gfunc);
    } else {
        const auto length = parseJSEvents(gfunc);
        if (length > 0) {
            saveSettings();
        }
    }
    ui->jsLog->hide();
    ui->tabWidget->show();

}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    if (index == 1 || !mNeedToRebuild) {

        on_epochBox_activated(ui->epochBox->currentIndex());
        return;
    }
    buildEventsMap();
    mNeedToRebuild = false;
}

void MainWindow::on_graphicsView_rubberBandChanged(const QRect &, const QPointF &, const QPointF &)
{
    // selection
}

void MainWindow::onSceneRectChanged(const QRectF &rect)
{
    ui->timelineMap->setVisibleRange( QDateTime(QDate(), QTime::fromMSecsSinceStartOfDay(rect.left())),
                                      QDateTime(QDate(), QTime::fromMSecsSinceStartOfDay(rect.right())) );
}

void MainWindow::onHorizontalScroll(int)
{

    static const QDate dt = QDate(2014, 1,1);
    const qreal ms1 = ui->graphicsView->mapToScene(ui->graphicsView->viewport()->geometry().topLeft()).x();
    const qreal ms2 = ui->graphicsView->mapToScene(ui->graphicsView->viewport()->geometry().topRight()).x();
    //qDebug() << pos << " t = "<< QDateTime(dt, QTime::fromMSecsSinceStartOfDay(pos/2.0));
    ui->timelineMap->setVisibleRange( QDateTime(dt, QTime::fromMSecsSinceStartOfDay(ms1/2.0)),
                                      QDateTime(dt, QTime::fromMSecsSinceStartOfDay(ms2/2.0)));

    ui->currentLabel->setText(QTime::fromMSecsSinceStartOfDay(ms1/2.0).toString("hh:mm:ss.zzz"));

}

void MainWindow::on_findNextButton_clicked()
{
    ui->plainTextEdit->find(ui->findLineEdit->text(), QTextDocument::FindCaseSensitively);
    ui->plainTextEdit->ensureCursorVisible();
}

void MainWindow::on_findPrevButton_clicked()
{
    ui->plainTextEdit->find(ui->findLineEdit->text(), QTextDocument::FindBackward|QTextDocument::FindCaseSensitively);
    ui->plainTextEdit->ensureCursorVisible();

}

void MainWindow::on_findLineEdit_returnPressed()
{
    on_findNextButton_clicked();
}

void MainWindow::on_epochBox_activated(int index)
{
    const SeriesMap &seriesMap = epochList.at(index);
    if (visibleSeriesID != &seriesMap) {
        ui->timelineMap->updateWithEvents(seriesMap);
        clearGraphicsScene();
        updateTimelineRangeStrings();
    }
}
