#include "qtimelinemap.h"
#include <QPaintEvent>
#include <QPainter>
#include <QDebug>
#include <math.h>

qtimelinemap::qtimelinemap(QWidget *parent) :
    QWidget(parent)
{
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(onContextMenuRequested(QPoint)));
    inrange = qMakePair(0.0, 1.0);
}

QPair<QDateTime, QDateTime> qtimelinemap::getSelectedRange() const
{
    qDebug() << "getSelected() full range is " << minMax;
    QPair<QDateTime, QDateTime> selected = minMax;
    const long long msecs = minMax.first.msecsTo(minMax.second);
    selected.second = minMax.first.addMSecs(msecs*inrange.second);
    selected.first  = minMax.first.addMSecs(msecs*inrange.first);
    return selected;
}

void qtimelinemap::setVisibleRange(const QDateTime &left, const QDateTime &right)
{
    visible.first = left;
    visible.second = right;
    update();
}

inline int msecFromDT(const QDateTime& dt)
{
    return dt.time().msecsSinceStartOfDay();// + 24.0*60*60*1000* dt.date().day();
}


void qtimelinemap::updateWithEvents(const SeriesMap &seriesMap)
{
    bool needInit = true;
    QList<int> all;
    foreach (ListOfEventGroups egs, seriesMap.values()) {
        foreach (EventsGroup eg, *egs) {
            foreach (LogEventP e, *eg) {
                if (e->timestamp.isValid()) {
                    if (needInit) {
                        minMax = qMakePair(e->timestamp, e->timestamp);
                        needInit = false;
                    }
                    all.append(msecFromDT(e->timestamp));
                    minMax.first = qMin(minMax.first, e->timestamp);
                    minMax.second = qMax(minMax.second, e->timestamp);
                }
            }
        }
    }
    qDebug() << "Full range is " << minMax;

    std::sort(all.begin(), all.end());
    if (all.isEmpty()) {
        return;
    }
    pm = QPixmap(contentsRect().size());
    QPainter p(&pm);
    const long min = all.first();
    const long max = all.last();
    viewport.first = min;
    viewport.second = max;
    const int width = pm.width();
    const int height = pm.height();
    const qreal toPixel = static_cast<qreal>(width)/(max-min);
    p.fillRect(QRect(0, 0, width, height), Qt::green);

    foreach (int i, all) {
        const qreal x = (i-min)*toPixel;
//        qDebug()<< min<<"  "<<max<<"  "<<x;
        p.drawLine(x, 0., x, height);
    }
    update();
//    qDebug()<< all;
}

void qtimelinemap::onContextMenuRequested(const QPoint &)
{
}

void qtimelinemap::wheelEvent(QWheelEvent *we)
{
    we->accept();
    const qreal timeDelta = we->pixelDelta().rx()?static_cast<qreal>(we->pixelDelta().rx())/width() : (static_cast<qreal>(we->pixelDelta().ry())/width())/2.0;

    if (static_cast<qreal>(we->position().x()) < static_cast<qreal>(width())/2.0) {
        inrange.first = qMax(0.0, qMin(inrange.second, inrange.first + timeDelta));
    } else {
        inrange.second = qMax(inrange.first, qMin(1.0, inrange.second + timeDelta));
    }
    update();
}

void qtimelinemap::paintEvent(QPaintEvent *pe)
{
    QWidget::paintEvent(pe);
    QPainter p(this);
    p.drawPixmap(contentsRect(), pm);
    {
        QColor c = Qt::yellow;
        c.setAlpha(180);
        p.setBrush(c);
        p.setPen(c);
        p.setBackground(c);
        QRect r = contentsRect();
        const int width = r.width();
        const qreal toPixel = static_cast<qreal>(width)/(viewport.second-viewport.first);
        r.setLeft ((msecFromDT(visible.first) -viewport.first)*toPixel);
        r.setRight((msecFromDT(visible.second)-viewport.first)*toPixel);
        p.drawRect(r);
    }

    {
        QColor c = Qt::magenta;
        c.setAlpha(180);
        p.setBrush(c);
        p.setPen(c);
        p.setBackground(c);
        QRect r = contentsRect();
        r.setRight(r.width()*inrange.first);
        p.drawRect(r);
    }

    {
        QColor c = Qt::magenta;
        c.setAlpha(180);
        p.setBrush(c);
        p.setPen(c);
        p.setBackground(c);
        QRect r = contentsRect();
        r.setLeft(r.width()*inrange.second);
        p.drawRect(r);
    }
}
