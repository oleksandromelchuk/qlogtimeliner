#ifndef LOGEVENT_H
#define LOGEVENT_H

#include <QDateTime>
#include <QSharedPointer>

/**
 * @brief The LogEvent class
 *
 * line - log line this event is associated with
 * timestamp - Date object
 * relation - id of events which has begin and end
 * Algorithm will try to find event with the same id in the list of previous events.
 * series - events with the same series value wil be placed in the special reserved line.
 * position - position of this event in duration-event [Begin, Mid, End]
 *
 */

class LogEvent
{
public:
    QString line;
    QDateTime timestamp;
    QString relation;
    QString series;
    QString position;

    QString description() const;

    bool isInRelation() const {
        return !relation.isEmpty();
    }
    bool isBegin() const {
        return !relation.isEmpty() && (position.compare("Begin")==0);
    }
    bool isMid() const {
        return !relation.isEmpty() && (position.compare("Mid")==0);
    }
    bool isEnd() const {
        return !relation.isEmpty() && (position.compare("End")==0);
    }
    bool isEpoch() const {
        return position.compare("Epoch")==0;
    }
};


Q_DECLARE_METATYPE(LogEvent);

typedef QSharedPointer<LogEvent> LogEventP;
typedef QSharedPointer<QList<LogEventP> > EventsGroup;
typedef QSharedPointer<QList< EventsGroup > > ListOfEventGroups;
typedef QMap<QString, ListOfEventGroups> SeriesMap;
typedef QList<SeriesMap> EpochList;


#endif // LOGEVENT_H
